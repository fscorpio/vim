" Casse la compatibilité avec vi (indispensable)
set nocompatible
set modeline modelines=5
set backspace=indent,eol,start

" Active la souris
set mouse=a

" conf pour surround
let g:surround_no_mappings = 1

" Indent guides
let g:indent_guides_enable_on_vim_startup = 1

" conf pour NERDCommenter
let g:NERDSpaceDelims = 1

" Pathogen (gestionnaire de plugin)
call pathogen#infect()

" Leader
let mapleader="à"
set hidden

" Coloration syntaxique
syntax on
" Fichier de conf couleur dans ~/.vim/colors/ ou dans bundle
colorscheme distinguished
set background=dark

" Pour l’indentation
set softtabstop=-1
set tabstop=4
set shiftwidth=4
set expandtab
set autoindent

" Show spaces
set listchars=nbsp:°,tab:>-,trail:·
set list

" Ne pas couper les lignes
set nowrap

" Les numéros de ligne dans la marge
set number

" Replier selon les markers
set foldmethod=marker
set foldmarker={{{,}}}
set foldlevelstart=99

" Affiche la position du curseur en bas à droite
set ruler

" Toujours trois lignes de visibles autour du curseur
set scrolloff=3

" Coloration lors de la recherche
set incsearch
set hlsearch

" Silence
set noerrorbells

" Auto-indentation intelligente
filetype on
filetype plugin on
filetype indent on

" spell en/fr
setlocal spelllang=en,fr

" Space, space = décolore la recherche
nnoremap <silent> <space><space> :nohlsearch<bar>:echo<CR>

" Space, s = supprime les espaces en fin de lignes
nnoremap <silent> <space>s :%s/\s\+$//e<bar>:echo<CR>

" Space, r = toggle spell
nnoremap <silent> <space>r :setlocal spell!<bar>:echo<CR>

" Space, i = toggle indent guide
nnoremap <silent> <space>i :IndentGuidesToggle<bar>:echo<CR>

imap jj <esc>
vmap jj <esc>

" BÉPO
noremap c h
noremap C H
noremap t j
noremap T J
noremap s k
noremap S K
noremap r l
noremap R L

noremap h t
noremap H T
noremap j q
noremap J Q
noremap k $
noremap K #
noremap l c
noremap L C

noremap q s
noremap Q S
noremap g r
noremap G R

noremap gx rx

noremap é w
noremap É W
noremap è g
noremap È G

noremap ç %
noremap Ç `

noremap « <
noremap » >
noremap! « <
noremap! » >
noremap g« r<
noremap g» r>
noremap '« '<
noremap '» '>

noremap < «
noremap > »
noremap! < «
noremap! > »
noremap g< r«
noremap g> r»
noremap '< '«
noremap '> '»

noremap èè gg

onoremap ié iw
onoremap iÉ iW
onoremap aé aw
onoremap aÉ aW

onoremap i» i>
onoremap i« i<
onoremap a» a>
onoremap a« a<

vnoremap ié iw
vnoremap iÉ iW
vnoremap aé aw
vnoremap aÉ aW

vnoremap i» i>
vnoremap i« i<
vnoremap a» a>
vnoremap a« a<

noremap , ;
noremap ; ,

" fun
noremap … ...

noremap zs zk
noremap zt zj

" Easily C-w
nnoremap w <C-w>

" Easily switch between windows
nnoremap <silent> <Esc>c :wincmd h<CR>
nnoremap <silent> <Esc>t :wincmd j<CR>
nnoremap <silent> <Esc>s :wincmd k<CR>
nnoremap <silent> <Esc>r :wincmd l<CR>

" Easily resize window
nnoremap <silent> <Esc>- :wincmd -<CR>
nnoremap <silent> <Esc>+ :wincmd +<CR>
nnoremap <silent> <Esc>« :wincmd <<CR>
nnoremap <silent> <Esc>» :wincmd ><CR>

" Increment
nnoremap <C-y> <C-a>

" Keyword completion
inoremap <C-j> <C-p>

" Surround map
nmap dq  <Plug>Dsurround
nmap lq  <Plug>Csurround
nmap lQ  <Plug>CSurround
nmap yq  <Plug>Ysurround
nmap yQ  <Plug>YSurround
nmap yqq <Plug>Yssurround
nmap yQq <Plug>YSsurround
nmap yQQ <Plug>YSsurround
xmap Q   <Plug>VSurround
xmap èQ  <Plug>VgSurround

" Fix vim git bash
augroup vimStartup
autocmd! vimStartup BufRead

" Autoclose YCM windows
autocmd CompleteDone * pclose

" Fix vim git bash cursor: https://cygwin.com/ml/cygwin/2013-03/msg00027.html
let &t_ti.="\e[1 q"
let &t_SI.="\e[5 q"
let &t_EI.="\e[1 q"
let &t_te.="\e[0 q"

" vim: set ft=vim ts=4 sw=4 et:
