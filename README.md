# README #

Ma conf pour vim.  

Pour l’installation :

```
#!bash

ln -Ts "$(pwd -P)/vim" "${HOME}/.vim"
ln -Ts "$(pwd -P)/vimrc" "${HOME}/.vimrc"
ln -Ts "$(pwd -P)/vimrc_azerty" "${HOME}/.vimrc_azerty"
ln -Ts "$(pwd -P)/ideavimrc" "${HOME}/.ideavimrc"
ln -Ts "$(pwd -P)/vrapperrc" "${HOME}/.vrapperrc"
```

Où lancer simplement le script install.sh.

Puis initialiser les sous-modules :

```
#!bash

git submodule update --init
```

Puis mettre en place un hook :

```
#!bash

ln -Ts ../../hooks/post-merge .git/hooks/post-merge
```

Où lancer simplement le script install\_hooks.sh.

♡ Copying is an act of love. Love is not subject to law.
