#!/bin/bash
# vim: set ft=sh sts=4 sw=4 et:

cd "$(dirname "${BASH_SOURCE[0]}")" && {
    ln -Ts "$(pwd -P)/vim" "${HOME}/.vim"
    ln -Ts "$(pwd -P)/vimrc" "${HOME}/.vimrc"
    ln -Ts "$(pwd -P)/vimrc_azerty" "${HOME}/.vimrc_azerty"
    ln -Ts "$(pwd -P)/ideavimrc" "${HOME}/.ideavimrc"
    ln -Ts "$(pwd -P)/vrapperrc" "${HOME}/.vrapperrc"
}
