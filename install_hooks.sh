#!/bin/bash
# vim: set ft=sh sts=4 sw=4 et:

cd "$(dirname "${BASH_SOURCE[0]}")" && {
    ln -Ts ../../hooks/post-merge .git/hooks/post-merge
}
